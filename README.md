# Backend - Medidor de Maré

## Dependências

O sistema tem como dependências no sistema:

1. Postgresql
2. nodejs

Após instalá-las, é necessário instalar os node modules com o comando:

```npm install```

## Desenvolvimento

### Configuração inicial

Para o desenvolvimento, recomenda-se utilizar o vscode, aceitando todas as extensões recomendadas.

O sistema precisa de algumas variáveis de ambiente para funcionar, que no desenvolvimento devem ficar em um arquivo ```.env``` por comodidade. Para uma configuração inicial existe um arquivo ```.env.example``` no repositório, então faça uma cópia deste arquivo para um arquivo ```.env```. As credenciais presentes no arquivo jamais serão utilizadas em produção.

Para conseguir testar funcionalidades também é necessário ter alguns dados no banco de dados. Para isso, primeiro utilize o comando ```npx prisma migrate dev```, e se precisar apagar dados inseridos previamente utiliza o comando ```npx prisma migrate reset```. Depois use o comando ```npm run mocks``` para aplicar os mocks ao banco de dados.

### Execução

Para rodar o sistema em desenvolvimento:

```npm run dev```

### Sugestão de ferramentas

Para o desenvolvimento recomenda-se as seguintes ferramentas a mais:

1. ```npx prisma studio```: este comando abrirá uma página web de visualização e manipulação do banco de dados da aplicação.
2. Postman: programa pra fazer requisições manualmente e simular servidores respondendo a requisições.

### Ciclo de desenvolvimento do banco de dados

No desenvolvimento de funcionalidades será necessário mudar modelos no banco de dados, fazer migrações, limpar os dados, etc.

As alterações no banco devem ser feitas alterando o arquivo ```prisma/schema.prisma```. Depois, para serem aplicadas, usa-se o comando 

```npx prisma migrate dev```

Este comando cria pastas com arquivos de comandos SQL que fazem as mudanças no banco. Enquanto a alteração não tiver o efeito desejado, estas novas pastas podem ser apagadas e o banco trazido de volta para a versão anterior com o comando (este comando também apaga os dados):

```npx prisma migrate reset```

Com o banco reiniciado, o processo de alteração e teste pode ser feito novamente. Quando se alcançar a configuração desejada as alterações podem ser adicionadas ao versionamento. 
