FROM node:14
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE ${BACKEND_PORT}
EXPOSE 5555
RUN npx prisma generate
CMD npm start