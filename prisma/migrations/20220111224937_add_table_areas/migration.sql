-- AlterTable
ALTER TABLE "Device" ADD COLUMN     "altitude" DOUBLE PRECISION NOT NULL DEFAULT 0,
ADD COLUMN     "area_id" INTEGER NOT NULL DEFAULT 0;

-- CreateTable
CREATE TABLE "Area" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "Area_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Area_name_key" ON "Area"("name");

-- AddForeignKey
ALTER TABLE "Device" ADD CONSTRAINT "Device_area_id_fkey" FOREIGN KEY ("area_id") REFERENCES "Area"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
