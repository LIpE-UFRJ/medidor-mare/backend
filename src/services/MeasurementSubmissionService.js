import jwt from 'jsonwebtoken';
import Hashes from 'jshashes';
import prisma from '../lib/prisma.js';

class MeasurementSubmissionService {
  async execute(deviceId, measurements) {
    for (let measurement of measurements) {
      if (measurement.type == 'distance') {
        await prisma.distance.create({
          data:
          {
            device_id: deviceId,
            timestamp: measurement.data.timestamp,
            value: measurement.data.median,
            dev: measurement.data.percentile_75 - measurement.data.percentile_25
          }
        });
      }
    }
  }
}

export { MeasurementSubmissionService }