import jwt from 'jsonwebtoken';
import Hashes from 'jshashes';
import prisma from '../lib/prisma.js';
import { getTideLevel } from '../lib/getTideLevel.js';

class GetMeasurementsService {
  async execute(type, source, startTime, endTime) {
    if (type === 'tideLevel') {
      return await getTideLevel(source, startTime, endTime);
    }
    else {
      throw Error("Tipo de medição inválido");
    }
  }
}

export { GetMeasurementsService }