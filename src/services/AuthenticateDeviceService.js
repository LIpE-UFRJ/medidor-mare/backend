import jwt from 'jsonwebtoken';
import Hashes from 'jshashes';
import prisma from '../lib/prisma.js';

var SHA512 = new Hashes.SHA512;

class AuthenticateDeviceService {
  async execute(deviceName, password) {
    const device = await prisma.device.findFirst({
      where: {
        name: deviceName
      }
    });

    if (!device) {
      throw Error("Dispositivo desconhecido");
    }

    if (SHA512.hex(password) != device.password) {
      throw Error("Senha inválida");
    }

    const token = jwt.sign(
      { deviceName },
      process.env.JWT_SECRET,
      {
        subject: `${device.id}`,
        expiresIn: "1d"
      }
    );

    return { token };
  }
}

export { AuthenticateDeviceService }