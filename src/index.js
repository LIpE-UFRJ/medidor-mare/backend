import { serverHttp } from "./app.js";

serverHttp.listen(
  process.env.BACKEND_PORT,
  () => console.log(
    `Server on ${process.env.NODE_ENV} is running on port ${process.env.BACKEND_PORT}`
  )
);
