import Prisma from '@prisma/client';
const { PrismaClient } = Prisma


if (process.env.NODE_ENV === 'development') {
  var prismaLog = {
    log: ['query']
  }
}
else {
  var prismaLog = {}
}

const prisma =
  global.prisma ||
  new PrismaClient(prismaLog);

if (process.env.NODE_ENV !== 'production') global.prisma = prisma;

export default prisma;