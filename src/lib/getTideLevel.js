import prisma from './prisma.js';

async function getTideLevel(source, startTime, endTime) {
  if (!source.areas && !source.devices) {
    throw Error("Formato inválido.");
  }

  var devices = await prisma.device.findMany({
    where: {
      OR: [
        {
          name: {
            in: source.devices
          }
        },
        {
          area: {
            name: {
              in: source.areas
            }
          }
        }
      ]
    },
    include: {
      distances: {
        where: {
          timestamp: {
            gte: startTime,
            lt: endTime
          }
        }
      }
    }
  });

  var tideLevels = [];

  for (let device of devices) {
    tideLevels.push(
      ...device.distances.map(
        (element) => {
          element.value = device.altitude - element.value;
          return element;
        }
      )
    );
  }

  return tideLevels;
}

export { getTideLevel };