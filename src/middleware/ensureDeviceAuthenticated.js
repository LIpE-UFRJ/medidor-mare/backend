import jwt from 'jsonwebtoken';

export function ensureDeviceAuthenticated(request, response, next) {
  const authToken = request.headers.authorization;

  if (!authToken) {
    return response.status(401).json({
      errorCode: "token.invalid",
    });
  }

  const [, token] = authToken.split(" ");

  try {
    const { sub } = jwt.verify(token, process.env.JWT_SECRET);

    request.deviceId = parseInt(sub);

    return next();
  } catch (err) {
    return response.status(401).json({ errorCode: "token.expired" });
  }
}