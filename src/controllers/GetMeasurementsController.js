import { GetMeasurementsService } from "../services/GetMeasurementsService.js";

class GetMeasurementsController {
  async handle(request, response) {
    const { type, source, startTime, endTime } = request.body;

    const service = new GetMeasurementsService();
    try {
      let measurements = await service.execute(type, source, startTime, endTime);
      return response.json({ measurements });
    } catch (err) {
      return response.json(err.message);
    }

  }
}

export { GetMeasurementsController }
