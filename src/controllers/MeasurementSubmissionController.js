import { MeasurementSubmissionService } from "../services/MeasurementSubmissionService.js";

class MeasurementSubmissionController {
  async handle(request, response) {
    const { resumedMeasurements } = request.body;

    const service = new MeasurementSubmissionService();
    try {
      await service.execute(request.deviceId, resumedMeasurements);
      return response.status(200).send();
    } catch (err) {
      return response.json(err.message);
    }

  }
}

export { MeasurementSubmissionController }
