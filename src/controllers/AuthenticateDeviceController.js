import { AuthenticateDeviceService } from "../services/AuthenticateDeviceService.js";

class AuthenticateDeviceController {
  async handle(request, response) {
    const { deviceName, password } = request.body;

    const service = new AuthenticateDeviceService();
    try {
      const result = await service.execute(deviceName, password);
      return response.json(result);
    } catch (err) {
      return response.json(err.message);
    }

  }
}

export { AuthenticateDeviceController }
