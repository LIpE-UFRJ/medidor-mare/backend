import { Router } from "express";
import { AuthenticateDeviceController } from "./controllers/AuthenticateDeviceController.js";
import { MeasurementSubmissionController } from "./controllers/MeasurementSubmissionController.js";
import { GetMeasurementsController } from "./controllers/GetMeasurementsController.js";
import { ensureDeviceAuthenticated } from "./middleware/ensureDeviceAuthenticated.js";

const router = Router();

router.post("/device/authenticate", new AuthenticateDeviceController().handle);
router.post("/device/measurement", ensureDeviceAuthenticated, new MeasurementSubmissionController().handle);
router.post("/getMeasurements", new GetMeasurementsController().handle);

export { router }