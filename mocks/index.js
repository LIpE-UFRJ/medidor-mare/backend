import prisma from '../src/lib/prisma.js';

var areas = [
  { name: 'Area 51' },
  { name: 'Area Teste' }
];

var devices = [
  {
    name: 'Sensor bonito',
    password: '9af861cff07ad43782eac2724551796e23c3bea57eb6db0151063767caaee4453fc8373204b18c53ca715a4368bc65c261a5700d9b11f5296e838b7486926b12', //SHA512 hash of "batata"
    altitude: 1.0,
    area_id: 1
  },
  {
    name: 'Sensor figurante',
    password: 'not password',
    altitude: 1.5,
    area_id: 1
  }
];

for (var area of areas) {
  await prisma.area.create({
    data: area
  });
}

for (var device of devices) {
  await prisma.device.create({
    data: device
  });
}

for (var i = 0; i < 100; i++) {
  await prisma.distance.create({
    data: {
      device_id: 1,
      timestamp: 0.0 + i * 10,
      value: 1.0 + i / 10,
      dev: 0.1
    }
  });
}
